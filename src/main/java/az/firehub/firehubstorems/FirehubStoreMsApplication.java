package az.firehub.firehubstorems;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class FirehubStoreMsApplication {

    public static void main(String[] args) {
        SpringApplication.run(FirehubStoreMsApplication.class, args);
    }

}
